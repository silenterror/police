#include <Arduino.h>

/*
  This application is a testing on the Arduino to simulate a police light setup with
  different patterns between the red and blue leds.
*/

const int LED_PIN_RED = 3;
const int LED_PIN_BLUE = 4;
int TIMER;

void setup() {
  pinMode(LED_PIN_RED, OUTPUT);
  pinMode(LED_PIN_BLUE, OUTPUT);
  Serial.begin(9600);
}

// pattenAlpha - strobes red three times and then blue three times
void patternAlpha() {
  Serial.println("patternAlpha");
  TIMER = 50;
  for (int i = 0; i < 7; i++) {
    digitalWrite(LED_PIN_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);                       // wait for a second
    digitalWrite(LED_PIN_RED, LOW);    // turn the LED off by making the voltage LOW
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);                       // wait for a second
    digitalWrite(LED_PIN_BLUE, LOW);    // turn the LED off by making the voltage LOW
    delay(TIMER);
  }
}

// patternBeta - turns on red on and off and then blue on and off alternating rapidly
void patternBeta() {
  Serial.println("patternBeta");
  TIMER = 50;
  for (int i = 0; i < 10; i++) {
    digitalWrite(LED_PIN_BLUE, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, LOW);    // turn the LED off by making the voltage LOW
    delay(TIMER);
    digitalWrite(LED_PIN_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);
    digitalWrite(LED_PIN_RED, LOW);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER); 
  }
                        // wait for a second
}

// patternCharlie - turns both lights on and off simultaneously slower timed
void patternCharlie() {
  Serial.println("patternCharlie");
  TIMER = 100;
  for (int i = 0; i < 6; i++) {
    digitalWrite(LED_PIN_BLUE, HIGH);
    digitalWrite(LED_PIN_RED, HIGH);
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, LOW);
    digitalWrite(LED_PIN_RED, LOW);
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, HIGH);
    digitalWrite(LED_PIN_RED, HIGH);
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, LOW);
    digitalWrite(LED_PIN_RED, LOW);
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, HIGH);
    digitalWrite(LED_PIN_RED, HIGH);
    delay(TIMER);
  }
}

  // patternDelta - slower on of alternating blue and red
  void patternDelta() {
  Serial.println("patternDelta");
  TIMER = 100;
  for (int i = 0; i < 6; i++) {
    digitalWrite(LED_PIN_BLUE, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);
    digitalWrite(LED_PIN_BLUE, LOW);    // turn the LED off by making the voltage LOW
    delay(TIMER);
    digitalWrite(LED_PIN_RED, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);
    digitalWrite(LED_PIN_RED, LOW);   // turn the LED on (HIGH is the voltage level)
    delay(TIMER);                       // wait for a second
  }
}

void loop() {
  for (int i = 0; i < 4; i++) {
    patternAlpha();
  }

  for (int i = 0; i < 6; i++) {
    patternBeta();
  }

  for (int i = 0; i < 2; i++) {
    patternCharlie();
  }

  for (int i = 0; i < 4; i++) {
    patternDelta();
  }
}
